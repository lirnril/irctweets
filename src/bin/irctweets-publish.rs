#![feature(type_ascription)]

use {
    std::{
        path::{Path, PathBuf},
        fs,
        io,
        iter,
        time,
    },
    anyhow::{Result, anyhow},
    rusqlite::types::ToSql,
    tokio::time::delay_for,
    tracing::{span, trace, error, info, Level},
};

#[derive(Debug, structopt::StructOpt)]
struct Args {
    #[structopt(short, long, default_value = "irctweets.toml")]
    config: PathBuf,
}

struct App {
    db: rusqlite::Connection,
    creds: egg_mode::Token,
}

#[derive(Debug, PartialEq, serde_derive::Deserialize)]
struct Config {
    db: PathBuf,
    twitter: TwitterConfig,
}

impl Config {
    fn load<P: AsRef<Path>>(path: &P) -> Result<Config> {
        let file_contents = fs::read_to_string(&path)?;
        let config = toml::from_str(&file_contents)?;
        Ok(config)
    }
}

#[derive(Debug, PartialEq, serde_derive::Deserialize)]
struct TwitterConfig {
    consumer_token: String,
    consumer_token_secret: String,
    access_token: String,
    access_token_secret: String,
}

enum Status {
    New = 0, // or null
    Retweeted = 1,
    Error = 2,
    SmallAccount = 3,
}

const MIN_FOLLOWERS: i32 = 500;

impl App {
    fn init_db(&self) -> Result<()> {
        self.db.execute("
            create table if not exists tweet (
                id integer primary key,
                tweet_id integer unique not null,
                retweet_id integer,
                error varchar,
                status integer
            )
        ", rusqlite::NO_PARAMS)?;

        Ok(())
    }

    fn get_new_tweets(&self, limit: i32) -> Result<Vec<u64>> {
        let mut stmt = self.db.prepare("
            select tweet_id
            from tweet
            where coalesce(status = ?, true)
            limit ?;
        ")?;

        let mut rows = stmt.query(&[&(Status::New as i32), &limit])?;
        let mut ids = Vec::new();
        while let Some(row) = rows.next()? {
            let id: i64 = row.get(0)?;
            ids.push(id as u64);
        }

        Ok(ids)
    }

    fn store_retweet_id(&self, tweet_id: u64, retweet_id: u64)
            -> Result<()> {
        let tweet_id = tweet_id as i64;
        let retweet_id = retweet_id as i64;
        self.db.execute("
            update tweet
            set retweet_id = ?,
                status = ?
            where tweet_id = ? and coalesce(status = ?, true)
        ", &[retweet_id, Status::Retweeted as i64, tweet_id,
            Status::New as i64])?;

        Ok(())
    }

    fn store_error(&self, tweet_id: u64, error: String)
            -> Result<()> {
        let tweet_id = tweet_id as i64;
        self.db.execute("
            update tweet
            set error = ?,
                status = ?
            where tweet_id = ? and coalesce(status = ?, true)
        ", &[&error as &dyn ToSql, &(Status::Error as i32),
        &tweet_id, &(Status::New as i32)])?;

        Ok(())
    }

    async fn store_status(&self, tweet_id: u64, status: Status)
            -> Result<()> {
        let tweet_id = tweet_id as i64;
        self.db.execute("
            update tweet
            set status = ?
            where tweet_id = ? and coalesce(status = ?, true)
        ", &[&(status as i32) as &dyn ToSql,
            &tweet_id, &(Status::New as i32)])?;

        Ok(())
    }

    async fn tick(&self) -> Result<()> {
        let tweet_ids = self.get_new_tweets(100)?;
        if tweet_ids.len() == 0 {
            return Ok(());
        }

        for tweet_id in tweet_ids {
            let span = span!(Level::INFO, "processing tweet", %tweet_id);
            let _enter = span.enter();

            // todo: this could be parallel but lol
            if let Err(e) = self.process_tweet(tweet_id).await {
                error!(%e, "couldn't retweet");
                self.store_error(tweet_id, e.to_string())?;
            }
        }

        Ok(())
    }

    async fn run(&self) -> Result<()> {
        loop {
            if let Err(e) = self.tick().await {
                error!(%e, "error during tick");
            }

            delay_for(time::Duration::from_secs(5)).await;
        }
    }

    async fn is_retweet_allowed(&self, tweet_id: u64) -> Result<bool> {
        let tweet = egg_mode::tweet::lookup(iter::once(tweet_id),
                &self.creds).await?
            .pop().ok_or_else(|| anyhow!("not found"))?;

        if tweet.retweeted == Some(true) {
            return Ok(false);
        }

        let user = tweet.user.ok_or_else(|| anyhow!("user missing"))?;

        if user.followers_count < MIN_FOLLOWERS {
            let relation = egg_mode::user::relation_lookup(iter::once(user.id),
                &self.creds).await?
                .pop().ok_or_else(|| anyhow!("relation lookup failed"))?;
            let they_follow_us = &relation.connections.iter()
                .any(|c| matches!(c, egg_mode::user::Connection::FollowedBy));

            if !they_follow_us {
                self.store_status(tweet_id, Status::SmallAccount).await?;
                return Ok(false);
            }
        }

        Ok(true)
    }

    async fn process_tweet(&self, tweet_id: u64) -> Result<()> {
        if !self.is_retweet_allowed(tweet_id).await? {
            return Ok(());
        }

        let r =
            egg_mode::tweet::retweet(tweet_id, &self.creds).await?;
        let retweet = r.response;
        info!(%retweet.id, "retweeted");
        self.store_retweet_id(tweet_id, retweet.id)?;

        Ok(())
    }
}

#[paw::main]
#[tokio::main]
async fn main(args: Args) -> Result<()> {
    let subscriber = tracing_subscriber::fmt::Subscriber::builder()
        .with_env_filter(tracing_subscriber::EnvFilter::from_default_env())
        .compact()
        .with_writer(io::stderr)
        .finish();
    tracing::subscriber::set_global_default(subscriber)?;
    trace!(?args, "starting up");

    let config = Config::load(&args.config)?;

    let creds = egg_mode::Token::Access {
        consumer: egg_mode::KeyPair::new(config.twitter.consumer_token,
            config.twitter.consumer_token_secret),
        access: egg_mode::KeyPair::new(config.twitter.access_token,
            config.twitter.access_token_secret),
    };

    let db = rusqlite::Connection::open(&config.db)?;

    let app = App { db, creds };

    app.init_db()?;

    app.run().await?;

    Ok(())
}
